package gflima;

import eu.larkc.csparql.cep.api.RdfQuadruple;
import eu.larkc.csparql.cep.api.RdfStream;
import eu.larkc.csparql.core.engine.ConsoleFormatter;
import eu.larkc.csparql.core.engine.CsparqlEngine;
import eu.larkc.csparql.core.engine.CsparqlEngineImpl;
import eu.larkc.csparql.core.engine.CsparqlQueryResultProxy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

public class App {

    // The name of this program.
    private static final String PROGNAME = "csparql-cli";

    // Global options:
    private static boolean optVerbose = false;

    private static void trace (String msg) {
        if (optVerbose) {
            System.err.println("trace: " + msg);
        }
    }

    private static void error (String msg) {
        System.err.println ("error: " + msg);
        System.exit (1);
    }

    private static void usage (String msg) {
        System.err.println
            (String.format ("%s: %s", PROGNAME, msg));
        System.err.println
            (String.format ("Try '%s --help' for more information.",
                            PROGNAME));
        System.exit (1);
    }

    private static String readFileAsString (String path) {
        try {
            byte[] encoded = Files.readAllBytes (Paths.get (path));
            return new String (encoded, StandardCharsets.UTF_8);
        } catch (IOException e) {
            error (String.format ("cannot access '%s'", path));
        }
        assert false; // unreachable
        return null;
    }

    private static BufferedReader readFileAsStream (String path) {
        try {
            if (path == "-") {
                return new BufferedReader
                    (new InputStreamReader (System.in));
            } else {
                return new BufferedReader
                    (new FileReader (path));
            }
        } catch (FileNotFoundException e) {
            error (String.format ("cannot access '%s'", path));
        }
        assert false; // unreachable
        return null;
    }

    private static long parseTime (String str) {
        String[] toks = str.split (":");
        if (toks.length == 1) {
            return Long.parseLong (toks[0]);
        } else if (toks.length == 3) {
            long h = Long.parseLong (toks[0]);
            long m = Long.parseLong (toks[1]);
            long s = Long.parseLong (toks[2]);
            h = (h > 0) ? h : 0;
            m = (m > 0) ? m : 0;
            s = (s > 0) ? s : 0;
            return (h * 3600 + m * 60 + s * 1) * 1000;
        } else {
            return -1;
        }
    }

    public static void main (String[] args) {

        // Configure Log4j.
        PropertyConfigurator.configure ("etc/log4j.properties");

        // Parse options.
        OptionParser parser = new OptionParser ();
        parser.accepts ("help");
        parser.accepts ("static").withRequiredArg ();
        parser.accepts ("stream-iri").withRequiredArg ();
        parser.accepts ("verbose");

        OptionSet opts = null;
        try {
            opts = parser.parse (args);
        } catch (OptionException e) {
            usage (e.getMessage ());
        }

        if (opts.has ("help")) {
            System.out.print
                ("Usage: csparql-cli [OPTION]... QUERY STREAM\n"
                 + "Apply QUERY to STREAM.\n\n"
                 + "With no STREAM, or if STREAM is -, read standard input.\n\n"
                 + "      --static=IRI      static model IRI\n"
                 + "      --stream-iri=IRI  stream IRI\n"
                 + "      --verbose         explain what is being done\n"
                 + "      --help            display this help and exit\n"
                 );
            System.exit (0);
        }

        if (opts.has ("verbose")) {
            optVerbose = true;
        }

        // Get operands.
        String pathToQuery = null;
        String pathToInput = "-";

        List<String> ell = (List<String>) opts.nonOptionArguments ();
        if (ell.size () == 0) {
            usage ("missing query operand");
        }
        if (ell.size () > 2) {
            usage ("too many operands");
        }
        pathToQuery = ell.get (0);
        if (ell.size () == 2) {
            pathToInput = ell.get (1);
        }

        // Load query and input.
        String query = readFileAsString (pathToQuery);
        BufferedReader input = readFileAsStream (pathToInput);

        // Create and initialize CSPARQL engine.
        CsparqlEngine engine = new CsparqlEngineImpl ();
        engine.initialize ();

        // Register static model (if any).
        if (opts.hasArgument ("static")) {
            String iri = (String) opts.valueOf ("static");
            assert iri != null;
            trace ("--static: " + iri);
            try {
                engine.putStaticNamedModel (iri, iri);
            } catch (Exception e) {
                error ("bad static model: " + iri);
            }
        }

        // Register input stream.
        String iri = opts.hasArgument("stream-iri")
            ? (String) opts.valueOf ("stream-iri") : "";
        RdfStream stream = new RdfStream (iri);
        engine.registerStream (stream);

        // Register query.
        CsparqlQueryResultProxy result = null;
        try {
            result = engine.registerQuery (query, false);
        } catch (Exception e) {
            error ("bad query: " + e.getMessage ());
        }
        assert result != null;
        result.addObserver (new ConsoleFormatter ());

        // Feed input file to engine's input stream.
        try {
            for (String line; (line = input.readLine ()) != null; ) {
                String[] toks = line.split (" +");
                if (toks.length != 4) {
                    error ("bad line: " + line);
                }
                long time = parseTime (toks[3]);
                if (time < 0) {
                    error ("bad time value: " + toks[3]);
                }
                RdfQuadruple q = new RdfQuadruple
                    (toks[0], toks[1], toks[2], parseTime (toks[3]));
                stream.put (q);
                trace (q.toString ());
            }
        } catch (IOException e) {
            error ("read error: " + e.getMessage ());
        }

        engine.unregisterQuery (result.getId ());
        engine.unregisterStream (stream.getIRI ());

        // Done.
        System.exit (0);
    }
}
